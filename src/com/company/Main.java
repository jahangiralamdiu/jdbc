package com.company;

import java.sql.*;

public class Main {

    public static void main(String[] args) {
        try {
            Connection connection = ConnectionUtils.getConnection();
            Statement statement = connection.createStatement();
            boolean isInserted = statement.execute("INSERT INTO pet\n" +
                    "VALUES ('33r','Diane','hamster','f','1999-03-30',NULL);");
            ResultSet result = statement.executeQuery("select * from pet");
            while (result.next()) {
                System.out.println(result.getString("name"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
