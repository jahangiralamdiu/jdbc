package com.company;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionUtils {

    private static Connection connection;

    public static Connection getConnection() throws SQLException {
        if (connection == null) {
            String url = "jdbc:mysql://localhost:3306/test_db";
            connection = DriverManager.getConnection(url, "test_user", "Dhaka@2020");
        }
        return connection;
    }
}
